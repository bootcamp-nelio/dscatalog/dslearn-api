INSERT INTO users (name, email, password) VALUES ('Alex Brown', 'alex@gmail.com', '$2a$10$eACCYoNOHEqXve8aIWT8Nu3PkMXWBaOxJ9aORUYzfMQCbVBIhZ8tG');
INSERT INTO users (name, email, password) VALUES ('Maria Green', 'maria@gmail.com', '$2a$10$eACCYoNOHEqXve8aIWT8Nu3PkMXWBaOxJ9aORUYzfMQCbVBIhZ8tG');
INSERT INTO users (name, email, password) VALUES ('Admin', 'admin@gmail.com', '$2a$10$eACCYoNOHEqXve8aIWT8Nu3PkMXWBaOxJ9aORUYzfMQCbVBIhZ8tG');

INSERT INTO notifications (text, moment, read, route, user_id) VALUES ('Olá, essa é uma msg de teste', TIMESTAMP WITH TIME ZONE '2022-07-02T11:34:00Z', false, '/api/notificação', 1);
INSERT INTO notifications (text, moment, read, route, user_id) VALUES ('Olá, essa é uma msg de teste 2', TIMESTAMP WITH TIME ZONE '2022-06-02T10:00:00Z', false, '/api/notificação', 2);

INSERT INTO roles (authority) VALUES ('ROLE_STUDENT');
INSERT INTO roles (authority) VALUES ('ROLE_INSTRUCTOR');
INSERT INTO roles (authority) VALUES ('ROLE_ADMIN');

INSERT INTO user_roles (user_id, role_id) VALUES (1, 1);
INSERT INTO user_roles (user_id, role_id) VALUES (2, 1);
INSERT INTO user_roles (user_id, role_id) VALUES (2, 2);
INSERT INTO user_roles (user_id, role_id) VALUES (3, 1);
INSERT INTO user_roles (user_id, role_id) VALUES (3, 2);
INSERT INTO user_roles (user_id, role_id) VALUES (3, 3);

INSERT INTO courses (name, image_uri, image_gray_uri) VALUES ('Bootcamp HTML','https://www.alura.com.br/artigos/assets/html-css-js/html-css-e-js-definicoes.png','https://www.alura.com.br/artigos/assets/html-css-js/imagem-1.png');

INSERT INTO offers (edition, start_moment, end_moment, course_id) VALUES ('1.0',TIMESTAMP WITH TIME ZONE '2020-11-20T03:00:00Z',TIMESTAMP WITH TIME ZONE '2021-11-20T03:00:00Z',1);
INSERT INTO offers (edition, start_moment, end_moment, course_id) VALUES ('2.0',TIMESTAMP WITH TIME ZONE '2020-12-20T03:00:00Z',TIMESTAMP WITH TIME ZONE '2021-12-20T03:00:00Z',1);

INSERT INTO resources (title, description, position, image_uri, type, offer_id) VALUES ('Trilha HTML', 'Trilha principal do curso', 1, 'https://www.alura.com.br/artigos/assets/html-css-js/html-css-e-js-definicoes.png', 1, 1);
INSERT INTO resources (title, description, position, image_uri, type, offer_id) VALUES ('Forum', 'Tire suas duvidas', 2, 'https://www.alura.com.br/artigos/assets/html-css-js/html-css-e-js-definicoes.png', 2, 1);
INSERT INTO resources (title, description, position, image_uri, type, offer_id) VALUES ('Lives', 'Lives exclusivas para a turma', 3, 'https://www.alura.com.br/artigos/assets/html-css-js/html-css-e-js-definicoes.png', 0, 1);

INSERT INTO sections (title, description, position, image_uri, resource_id, pre_requisite_id) VALUES ('Capítulo 1', 'Neste capítulo vamos começar', 1, 'https://www.alura.com.br/artigos/assets/html-css-js/html-css-e-js-definicoes.png', 1, null)
INSERT INTO sections (title, description, position, image_uri, resource_id, pre_requisite_id) VALUES ('Capítulo 2', 'Neste capítulo vamos continuar', 2, 'https://www.alura.com.br/artigos/assets/html-css-js/html-css-e-js-definicoes.png', 1, 1)
INSERT INTO sections (title, description, position, image_uri, resource_id, pre_requisite_id) VALUES ('Capítulo 3', 'Neste capítulo vamos finalizar', 3, 'https://www.alura.com.br/artigos/assets/html-css-js/html-css-e-js-definicoes.png', 1, 2)

INSERT INTO enrollments (user_id, offer_id, enroll_moment, refund_moment, available, only_update) VALUES (1, 1, TIMESTAMP WITH TIME ZONE '2020-11-20T13:00:00Z', null, true, false);
INSERT INTO enrollments (user_id, offer_id, enroll_moment, refund_moment, available, only_update) VALUES (2, 1, TIMESTAMP WITH TIME ZONE '2020-11-20T16:00:00Z', null, true, false);

INSERT INTO lessons (title, position, section_id) VALUES ('Aula 1 do capitulo 1', 1, 1);
INSERT INTO contents (id, text_content, video_uri) VALUES (1, '', 'https://www.youtube.com/watch?v=wzJTpzHHoVg');

INSERT INTO lessons (title, position, section_id) VALUES ('Aula 2 do capitulo 1', 2, 1);
INSERT INTO contents (id, text_content, video_uri) VALUES (2, '', 'https://www.youtube.com/watch?v=wzJTpzHHoVg');

INSERT INTO lessons (title, position, section_id) VALUES ('Aula 3 do capitulo 1', 3, 1);
INSERT INTO contents (id, text_content, video_uri) VALUES (3, '', 'https://www.youtube.com/watch?v=wzJTpzHHoVg');

INSERT INTO lessons (title, position, section_id) VALUES ('Tarefa do capitulo 1', 4, 1);
INSERT INTO tasks (id, description, question_count, approval_count, weight, due_date) VALUES (4, 'Fazer um trabalho legal', 5, 4, 1.0, TIMESTAMP WITH TIME ZONE '2020-11-25T13:00:00Z');

INSERT INTO lessons_done (lesson_id, user_id, offer_id) VALUES (1, 1, 1);
INSERT INTO lessons_done (lesson_id, user_id, offer_id) VALUES (2, 1, 1);

INSERT INTO deliveries (uri, moment, status, feedback, correct_Count, lesson_id, user_id, offer_id) VALUES ('https://github.com/devsuperior/bds-dslearn', TIMESTAMP WITH TIME ZONE '2020-12-10T10:00:00Z', 0, null, null, 4, 1, 1);


INSERT INTO topics (title, body, moment, author_id, offer_id, lesson_id) VALUES ('Título do tópico 1', 'Corpo do tópico 1', TIMESTAMP WITH TIME ZONE '2020-12-12T13:00:00Z', 1, 1, 1);
INSERT INTO topics (title, body, moment, author_id, offer_id, lesson_id) VALUES ('Título do tópico 2', 'Corpo do tópico 2', TIMESTAMP WITH TIME ZONE '2020-12-13T13:00:00Z', 2, 1, 1);
INSERT INTO topics (title, body, moment, author_id, offer_id, lesson_id) VALUES ('Título do tópico 3', 'Corpo do tópico 3', TIMESTAMP WITH TIME ZONE '2020-12-14T13:00:00Z', 2, 1, 1);
INSERT INTO topics (title, body, moment, author_id, offer_id, lesson_id) VALUES ('Título do tópico 4', 'Corpo do tópico 4', TIMESTAMP WITH TIME ZONE '2020-12-15T13:00:00Z', 1, 1, 2);
INSERT INTO topics (title, body, moment, author_id, offer_id, lesson_id) VALUES ('Título do tópico 5', 'Corpo do tópico 5', TIMESTAMP WITH TIME ZONE '2020-12-16T13:00:00Z', 1, 1, 2);
INSERT INTO topics (title, body, moment, author_id, offer_id, lesson_id) VALUES ('Título do tópico 6', 'Corpo do tópico 6', TIMESTAMP WITH TIME ZONE '2020-12-17T13:00:00Z', 2, 1, 3);

INSERT INTO topic_likes (topic_id, user_id) VALUES (1, 2);
INSERT INTO topic_likes (topic_id, user_id) VALUES (2, 1);

INSERT INTO replies (body, moment, topic_id, author_id) VALUES ('Tente reiniciar o computador', TIMESTAMP WITH TIME ZONE '2020-12-15T13:00:00Z', 1, 2);
INSERT INTO replies (body, moment, topic_id, author_id) VALUES ('Deu certo, valeu!', TIMESTAMP WITH TIME ZONE '2020-12-20T13:00:00Z', 1, 1);

INSERT INTO reply_likes (reply_id, user_id) VALUES (1, 1);
