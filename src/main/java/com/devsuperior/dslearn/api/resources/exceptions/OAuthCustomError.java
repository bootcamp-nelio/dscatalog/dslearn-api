package com.devsuperior.dslearn.api.resources.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class OAuthCustomError implements Serializable {

    private String error;

    @JsonProperty("error_description")
    private String errorDescription;


}
