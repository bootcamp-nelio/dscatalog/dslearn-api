package com.devsuperior.dslearn.api.resources;

import com.devsuperior.dslearn.api.dto.NotificationDTO;
import com.devsuperior.dslearn.api.services.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
@RequestMapping("/notifications")
@RequiredArgsConstructor
public class NotificationResources implements Serializable {

    private final NotificationService notificationService;

    @GetMapping("/{id}")
    public ResponseEntity<Page<NotificationDTO>> findById(Pageable pageable){
        final var dto = notificationService.notificationsForCurrentUser(pageable);
        return ResponseEntity.ok().body(dto);
    }
}
