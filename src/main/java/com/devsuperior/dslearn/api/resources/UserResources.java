package com.devsuperior.dslearn.api.resources;

import com.devsuperior.dslearn.api.dto.UserDTO;
import com.devsuperior.dslearn.api.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserResources implements Serializable {

    private final UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> findById(@PathVariable Long id){
        final var dto = userService.findById(id);
        return ResponseEntity.ok().body(dto);
    }
}
