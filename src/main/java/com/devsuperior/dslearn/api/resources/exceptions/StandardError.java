package com.devsuperior.dslearn.api.resources.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.Instant;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 2022/07/05
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StandardError implements Serializable {

  private Instant timestamp;
  private int status;
  private String error;
  private String message;
  private String path;

}
