package com.devsuperior.dslearn.api.services;

import com.devsuperior.dslearn.api.dto.DeliverRevisionDTO;
import com.devsuperior.dslearn.api.repositories.DeliverRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
@RequiredArgsConstructor
public class DeliverService implements Serializable {

    private final DeliverRepository deliverRepository;

    public void saveRevision(Long id, DeliverRevisionDTO dto) {
        final var deliver = deliverRepository.getOne(id);
        deliver.setStatus(dto.getStatus());
        deliver.setFeedback(dto.getFeedback());
        deliver.setCorrectCount(deliver.getCorrectCount());
        deliverRepository.save(deliver);
    }
}
