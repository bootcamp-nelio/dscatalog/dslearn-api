package com.devsuperior.dslearn.api.services;

import com.devsuperior.dslearn.api.dto.NotificationDTO;
import com.devsuperior.dslearn.api.repositories.NotificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Service
@RequiredArgsConstructor
public class NotificationService implements Serializable {

    private final NotificationRepository notificationRepository;

    private final AuthService authService;

    @Transactional(readOnly = true)
    public Page<NotificationDTO> notificationsForCurrentUser(Pageable pageable) {
        final var user = authService.authenticated();
        final var page = notificationRepository.findByUser(user, pageable);
        return page.map(NotificationDTO::new);
    }
}
