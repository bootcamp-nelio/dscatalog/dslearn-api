package com.devsuperior.dslearn.api.services.exceptions;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 2022/07/05
 */

public class ResourceNotFoundException extends RuntimeException {

  public ResourceNotFoundException(String message) {
	super(message);
  }

}
