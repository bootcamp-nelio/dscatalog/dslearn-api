package com.devsuperior.dslearn.api.services.exceptions;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 2022/07/05
 */

public class DatabaseException extends RuntimeException {

  public DatabaseException(String message) {
	super(message);
  }

}
