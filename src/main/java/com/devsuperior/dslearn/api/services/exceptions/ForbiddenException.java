package com.devsuperior.dslearn.api.services.exceptions;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 2022/07/19
 */

public class ForbiddenException extends RuntimeException {

  public ForbiddenException(String message) {
	super(message);
  }

}
