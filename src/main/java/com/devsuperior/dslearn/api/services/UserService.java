package com.devsuperior.dslearn.api.services;

import com.devsuperior.dslearn.api.dto.RoleDTO;
import com.devsuperior.dslearn.api.dto.UserDTO;
import com.devsuperior.dslearn.api.dto.UserInsertDTO;
import com.devsuperior.dslearn.api.dto.UserUpdateDTO;
import com.devsuperior.dslearn.api.entities.Role;
import com.devsuperior.dslearn.api.entities.User;
import com.devsuperior.dslearn.api.repositories.RoleRepository;
import com.devsuperior.dslearn.api.repositories.UserRepository;
import com.devsuperior.dslearn.api.services.exceptions.DatabaseException;
import com.devsuperior.dslearn.api.services.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.io.Serializable;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final UserRepository repository;

    private final AuthService authService;

    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Transactional(readOnly = true)
    public Page<UserDTO> findAllPaged(Pageable page) {
        var list = repository.findAll(page);
        return list.map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public UserDTO findById(Long id) {
        authService.validateSelfOrAdmin(id);
        return new UserDTO(repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found")));
    }

    @Transactional
    public UserDTO save(UserInsertDTO userDTO) {
        var user = new User();
        copyDtoToEntity(userDTO, user);
        var password = passwordEncoder.encode(userDTO.getPassword());
        user.setPassword(password);
        user = repository.save(user);
        return new UserDTO(user);
    }

    @Transactional
    public UserDTO update(Long id, UserUpdateDTO userDTO) {
        try {
            var user = repository.getOne(id);
            copyDtoToEntity(userDTO, user);
            user = repository.save(user);
            return new UserDTO(user);
        } catch(EntityNotFoundException e) {
            throw new ResourceNotFoundException("Id not found " + id);
        }
    }

    public void delete(Long id) {
        try {
            repository.deleteById(id);
        } catch(EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException("Id not found");
        } catch(DataIntegrityViolationException e) {
            throw new DatabaseException("Integrity violation");
        }
    }

    private void copyDtoToEntity(UserDTO userDTO, User user) {

        user.setName(userDTO.getName());
        user.setEmail(userDTO.getEmail());

        user.getRoles().clear();
        for(RoleDTO roleDTO : userDTO.getRoles()) {
            Role role = roleRepository.getOne(roleDTO.getId());
            user.getRoles().add(role);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = repository.findByEmail(username).orElseThrow(() -> {
            logger.error("User not found: " + username);
            return new UsernameNotFoundException("Email not found");
        });
        final var message = "User found: " + username;
        logger.info(message);
        return user;
    }

}
