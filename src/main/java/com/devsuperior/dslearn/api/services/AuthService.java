package com.devsuperior.dslearn.api.services;

import com.devsuperior.dslearn.api.entities.User;
import com.devsuperior.dslearn.api.repositories.UserRepository;
import com.devsuperior.dslearn.api.services.exceptions.ForbiddenException;
import com.devsuperior.dslearn.api.services.exceptions.ResourceNotFoundException;
import com.devsuperior.dslearn.api.services.exceptions.UnauthorizedException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
@RequiredArgsConstructor
public class AuthService implements Serializable {

    private final UserRepository userRepository;

    public User authenticated() {
        try {
            final var username = SecurityContextHolder.getContext().getAuthentication().getName();
            return userRepository.findByEmail(username).orElseThrow(() -> new ResourceNotFoundException("Entity not found"));
        } catch (Exception e) {
            throw new UnauthorizedException("Invalid user");
        }
    }

    public void validateSelfOrAdmin(Long id) {
        final var user = authenticated();
        if(!user.getId().equals(id) && !user.hasRole("ROLE_ADMIN")) throw new ForbiddenException("Access denied");
    }
}
