package com.devsuperior.dslearn.api.dto;

import com.devsuperior.dslearn.api.entities.Deliver;
import com.devsuperior.dslearn.api.enums.DeliverStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class DeliverRevisionDTO implements Serializable {

    private DeliverStatus status;
    private String feedback;
    private Integer correctCount;

    public DeliverRevisionDTO(Deliver deliver) {
        this.status = deliver.getStatus();
        this.feedback = deliver.getFeedback();
        this.correctCount = deliver.getCorrectCount();
    }
}
