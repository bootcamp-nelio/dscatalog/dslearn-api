package com.devsuperior.dslearn.api.dto;

import com.devsuperior.dslearn.api.entities.Notification;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class NotificationDTO implements Serializable {

    private Long id;
    private String text;
    private Instant moment;
    private Boolean read;
    private String route;
    private Long userId;

    public NotificationDTO(Notification notification) {
        this.id = notification.getId();
        this.text = notification.getText();
        this.moment = notification.getMoment();
        this.read = notification.getRead();
        this.route = notification.getRoute();
        this.userId = notification.getUser().getId();
    }
}
