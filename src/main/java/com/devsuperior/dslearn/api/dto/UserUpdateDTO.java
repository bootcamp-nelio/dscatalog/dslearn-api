package com.devsuperior.dslearn.api.dto;

import com.devsuperior.dslearn.api.services.validation.UserUpdateValid;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@UserUpdateValid
public class UserUpdateDTO extends UserDTO { }
