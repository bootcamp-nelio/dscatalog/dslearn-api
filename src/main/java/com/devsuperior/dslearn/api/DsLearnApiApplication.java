package com.devsuperior.dslearn.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DsLearnApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DsLearnApiApplication.class, args);
    }

}
