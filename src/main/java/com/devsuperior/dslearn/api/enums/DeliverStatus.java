package com.devsuperior.dslearn.api.enums;

public enum DeliverStatus {

    PENDING, ACCEPTED, REJECTED

}
