package com.devsuperior.dslearn.api.enums;

public enum ResourceType {

    LESSON_ONLY,
    LESSON_TASK,
    FORUM,
    EXTERNAL_LINK;

}
