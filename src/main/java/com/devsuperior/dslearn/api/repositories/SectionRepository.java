package com.devsuperior.dslearn.api.repositories;

import com.devsuperior.dslearn.api.entities.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 2022/07/05
 */

@Repository
public interface SectionRepository extends JpaRepository<Section, Long> {}
